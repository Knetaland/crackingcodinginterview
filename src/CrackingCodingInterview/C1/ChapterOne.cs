﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrackingCodingInterview.C1
{
    public class ChapterOne
    {

        /// <summary>
        /// 1_1
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool CheckIfAllCharsAreUnique(string s)
        {
            if (String.IsNullOrEmpty(s)) return true;

            Dictionary<char, bool> charMap = new Dictionary<char, bool>();

            for(int i =0; i < s.Length; i++)
            {
                if (charMap.ContainsKey(s[i])) return false;

                charMap.Add(s[i], true);
            }
            return true;
        }
    }
}
