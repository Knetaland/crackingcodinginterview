﻿using CrackingCodingInterview.C1;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CrackingCodingInterview
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var sw = Stopwatch.StartNew();
            sw.Start();
            P1_1();
            sw.Stop();
            Console.WriteLine("Elapsed time:" + sw.ElapsedMilliseconds + "ms");
            Console.ReadLine();

        }


        public static void P1_1()
        {
            var input = "aa";
            var isUnique =ChapterOne.CheckIfAllCharsAreUnique(input);

            Console.WriteLine(String.Format("{0} is unique {1}", input, isUnique));
        }
    }
}
